<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";
    protected $guarded = [];
    public $timestamps = false;

    // protected $fillable = ["judul", "isi", "tanggal_dibuat", "tanggal_diperbaharui"];
    // ini untuk one to one inverse ke users

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
