<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $guarded = [];
    //ini untuk one to many inverse ke books
    public function book()
    {
        return $this->belongsTo('App\Book');
    }
    
    //ini untuk one to one ke bookmarks_users
    public function bookmark_user()
    {
        return $this->hasOne('App\Bookmark_User');
    }

    //ini untuk one to one ke bookmarks_users
    public function previous()
    {
        $result = $this->book->chapters->where('id', '<', $this->id);
        return $result->last();
    }

    //ini untuk one to one ke bookmarks_users
    public function next()
    {
        $result = $this->book->chapters->where('id', '>', $this->id);
        return $result->first();
    }

}
