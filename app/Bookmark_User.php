<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark_User extends Model
{
    protected $table = 'bookmarks_users';
    protected $guarded = [];
    public $timestamps = false;
    protected $attributes = [
        'chapter_id' => null,
    ];

    //ini untuk one to one inverse ke chapters
    public function chapter()
    {
        return $this->belongsTo('App\Chapter');
    }
}
