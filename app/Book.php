<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Bookmark_User;

class Book extends Model
{
    protected $guarded = [];
    // protected $attributes = [
    //     'author_id' => '',
    // ];
    //ini untuk one to many ke chapters
    public function chapters()
    {
        return $this->hasMany('App\Chapter')->orderby('published_date');
    }
    //ini untuk many to many ke tags
    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'books_tags');
    }
    //ini untuk one to many inverse ke users
    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    //ini untuk many to many ke users
    public function users()
    {
        return $this->belongsToMany('App\User', 'bookmarks_users');
    }

    //ini untuk check kalo buku di bookmark oleh user
    public function bookmarked($user_id)
    {
        return $this->users->find(User::find($user_id));
    }

    //ini untuk check last read chapter oleh user
    public function lastRead($user_id)
    {

        $bookmark = Bookmark_User::where([
            ['user_id', $user_id],
            ['book_id', $this->id]
            ])->first();
            
        if ($bookmark){
            return $bookmark->chapter;
        } else {
            return null;
        }
    }

    //ini untuk check latest chapter oleh user
    public function latestChapters()
    {
        return $this->chapters->last();
    }

}
