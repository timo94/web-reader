<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    //ini untuk one to many ke books
    public function writtenBooks()
    {
        return $this->hasMany('App\Book', 'author_id');
    }
    //ini untuk one to one ke profiles
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
    //ini untuk many to many ke users
    public function bookmarks()
    {
        return $this->belongsToMany('App\Book', 'bookmarks_users');
    }
}
