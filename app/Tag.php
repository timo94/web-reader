<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];
    //ini untuk many to many ke books
    public function books()
    {
        return $this->belongsToMany('App\Book', 'books_tags');
    }
}
