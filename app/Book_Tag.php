<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book_Tag extends Model
{
    protected $guarded = [];
    protected $table = 'books_tags';
}
