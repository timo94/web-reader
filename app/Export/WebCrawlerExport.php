<?php

namespace App\Export;

use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

class WebCrawlerExport
{
    public static function fetchBookData($url) {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $crawler->filter('div.post-title > h1 > span')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            };
        });
        $book = [];
        $book['title'] = $crawler->filter('div.post-title > h1')->first()->text();
        $book['authors'] = $crawler->filter('div.author-content > a')->each(function ($node) {
            return $node->text();
        });

        $book['summary'] = join("", array_filter($crawler->filter('div.summary__content > p')->each(function ($node) {
            $safe = True;
            $str = $node->text();
            foreach( ['wuxiaworld.', 'disclaimer']  as $txt){
                if (stripos( strtolower($str), $txt) !== false) {
                    $safe = false;
                    break;
                }
            }
            if ($safe) {
                return "<p>". $str ."</p>";
            }
        })));

        $book['genres'] = $crawler->filter('div.genres-content > a')->each(function ($node) {
            return $node->text();
        });
        $book['chapters'] = $crawler->filter('ul.version-chap > li > a')->each(function ($node) {
            return array('title' => $node->text(), 'url' => $node->attr('href'));
        });
        $book['source'] = $url;
        return $book;
    }

    public static function fetchChapterContent($url) {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $crawler->filter('div.text-left > p > a')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            };
        });
        $chapter = [];
        $chapter['title'] = $crawler->filter('ol.breadcrumb > li.active')->first()->text();
        $chapter['content'] = join("", array_filter($crawler->filter('div.text-left > p')->each(function ($node) {
            return "<p>". $node->text() ."</p>";
        })));
        return $chapter;
    }
}