<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Auth;
use App\Book;
use App\Book_Tag;
use App\Bookmark_User;
use App\Chapter;
use RealRashid\SweetAlert\Facades\Alert;

class ChapterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('create', 'edit', 'update', 'store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // filter published where('published_date', '<', now())->
        $chapters = Chapter::orderby('published_date', 'desc')->get();
        return view('chapter.index', compact('chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('chapter.create', ['book' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $chapter = Chapter::create([
            'title' => $request['title'],
            'content' => $request['content'],
            'published_date' => $request['published_date'],
            'book_id' => $id
        ]);
        Alert::success('Berhasil', 'Berhasil Menambah Chapter Baru');

        return redirect(route('chapter.show', ['book' => $id, 'chapter' => $chapter] ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($book_id, $id)
    {
        if (Auth::user()){
            $bookmark = Bookmark_User::where([
                ['user_id', Auth::user()->id],
                ['book_id', $book_id]
                ])->first();
            if ($bookmark){
                $bookmark->update([
                    'user_id' => Auth::user()->id,
                    'book_id' => $book_id,
                    'chapter_id' => $id
                ]);
            }
        }
        $chapter = Chapter::find($id);
        return view('chapter.show', compact('chapter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($book_id, $id)
    {
        $chapter = Chapter::find($id);
        return view('chapter.edit', compact('chapter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $book_id, $id)
    {
        $chapter = Chapter::find($id)->update([
            'title' => $request['title'],
            'content' => $request['content'],
            'published_date' => $request['published_date']
        ]);
        Alert::success('Berhasil', 'Berhasil Mengedit Chapter');
        return redirect(route('chapter.show', ['book' => $book_id, 'chapter' => $id] ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($book_id, $id)
    {
        $bookmarks = Bookmark_User::where([
                ['book_id', $book_id],
                ['chapter_id', $id]
            ])->update([
                'chapter_id' => null
            ]);
        Chapter::destroy($id);
        Alert::success('Berhasil', 'Berhasil Menghapus Chapter');
        return redirect(route('books.show', ['book' => $book_id] ));
    }
}
