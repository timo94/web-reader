<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')
            ->only(['edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::all();
        return view('profile.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user = User::where('name', $username)->first();
        return view('profile.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        // Dapetin user pertama dengan username = $username
        $user = User::where('name', $username)->first();


        if (Auth::user() == $user) {
            // Kalo misalnya yang login merupakan user dia bisa edit profil
            return view('profile.edit', compact('user'));
        } else {
            // Kalo bukan ga bisa
            return view('profile.show', compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        // Dapetin user pertama dengan username = $username
        $user = User::where('name', $username)->first();

        // Kalo misalnya input usernamenya berubah perlu di cek usernamenya sudah kepakai atau belum
        // Kalo input name nya valid baru update username
        if ($user->name != $request["name"]){
            $request->validate([
                'name' => 'required|string|max:255|unique:users',
            ]);

            $user->update([
                "name" => $request["name"]
            ]);
        }

        // Update User profile nya sesuai input
        $profile = $user->profile->update([
            "photo" => $request["photo"],
            "fullname" => $request["fullname"],
            "about" => $request["about"]
        ]);

        Alert::success('Success', 'Profile Successfully Saved!');
        return redirect(route('profile.show', ['profile'=>$user->name]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
