<?php

namespace App\Http\Controllers;
// use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Http\Request;
use App\Tag;
use Auth;
use App\Book;
use App\Chapter;
use App\Book_Tag;
use App\Bookmark_User;
use DB;
use RealRashid\SweetAlert\Facades\Alert;


class BookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only('create', 'edit', 'update', 'store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return view('book.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag_ids = [];
        $tags_arr = explode(',', $request["tags"]);
        foreach ($tags_arr as $tag_name) {
            $tag = Tag::firstOrCreate(['name' => $tag_name]);
            $tag_ids[] = $tag->id;
        }
        $book = Book::create([
            'title' => $request['title'],
            'summary' => $request['summary'],
            'author_id' => Auth::id()
        ]);

        $book->tags()->sync($tag_ids);
        Alert::success('Berhasil', 'Berhasil Menambah Buku Baru');

        return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);
        return view('book.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $book = Book::find($id);
        if ($book->author_id == $user->id) {
            return view('book.edit', compact('book'));
        } else {
            Alert::warning('Error', 'You cant Edit other people book');
            return view('book.show', compact('book'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Book::where('id', $id)->update([
            'title' => $request['title'],
            'summary' => $request['summary']
        ]);
        Alert::success('Berhasil', 'Berhasil Mengedit Buku');
        return redirect('/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $book = Book::find($id);
        
        if ($book->author_id == $user->id) {
            $booktag = Book_Tag::where('book_id', $book->id)->delete();
            $chapter = Chapter::where('book_id', $book->id)->delete();
            $bookmarks = Bookmark_User::where('book_id', $book->id)->delete();


            Book::destroy($id);
            Alert::success('Berhasil', 'Berhasil Menghapus Buku');
            return redirect('/books');
        } else {
            Alert::warning('Error', 'You cant Delete other people book');
        }
    }

    public function saveBookmark(Request $request)
    {
        $bookmark_check = Bookmark_User::where([
            ['user_id', Auth::user()->id],
            ['book_id', $request['book_id']]
            ])->first();
            // dd($bookmark_check);
        if (!$bookmark_check) {
            $bookmark_user = Bookmark_User::create([
                'user_id' => Auth::user()->id,
                'book_id' => $request['book_id'],
                'chapter_id' => $request['chapter_id'] ?? null
            ]);
            Alert::success('Berhasil', 'Berhasil menyimpan bookmark');
        } else {
            Alert::warning('Gagal', 'Sudah ada di daftar bookmark anda');
        }

        if ($request['chapter_id'] != ''){
            $book = Book::find($request['book_id']);
            $chapter = Chapter::find($request['chapter_id']);
            return redirect(route('chapter.show', ['book' => $book, 'chapter' => $chapter]));
        } else {
            $book = Book::find($request['book_id']);
            return redirect(route('books.show', ['book' => $book]));
        }
    }

    public function removeBookmark(Request $request)
    {
        $bookmark_check = Bookmark_User::where([
            ['user_id', Auth::user()->id],
            ['book_id', $request['book_id']]
            ])->delete();
        
        if ($request['chapter_id'] != ''){
            $book = Book::find($request['book_id']);
            $chapter = Chapter::find($request['chapter_id']);
            return redirect(route('chapter.show', ['book' => $book, 'chapter' => $chapter]));
        } else {
            $book = Book::find($request['book_id']);
            return redirect(route('books.show', ['book' => $book]));
        }
    }
}
