<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Export\WebCrawlerExport;
use App\Book;
use App\Chapter;
use App\Tag;

class WebCrawlerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('webcrawler.index');
    }


    public function fetchBook(Request $request)
    {
        if ($request['link']){
            $book = WebCrawlerExport::fetchBookData($request['link']);
            return view('webcrawler.book', compact('book'));
        } else {
            return view('webcrawler.index');
        }
    }

    public function saveBook(Request $request)
    {
        $data = json_decode($request['book']);
        
        $tag_ids = [];
        foreach ($data->genres as $tag_name) {
            $tag = Tag::firstOrCreate(['name' => $tag_name]);
            $tag_ids[] = $tag->id;
        }
        $book = Book::create([
            'title' => $data->title,
            'summary' => $data->summary,
            'author_id' => Auth::id()
        ]);
        $book->tags()->sync($tag_ids);
        foreach($data->chapters as $link){
            $chapter_crawl = WebCrawlerExport::fetchChapterContent($link);
            $chapter = Chapter::create([
                'title' => $chapter_crawl['title'],
                'content' => $chapter_crawl['content'],
                'published_date' => now(),
                'book_id' => $book->id
            ]);
        }

        return redirect(route('books.show', ['book' => $book->id] ));
    }

    public function fetchChapter($link)
    {
        if ($link){
            $chapter = WebCrawlerExport::fetchChapterContent($link);
            return view('webcrawler.chapter', compact('chapter'));
        } else {
            return view('webcrawler.index');
        }
    }
}
