<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ChapterController@index');

Auth::routes();

//untuk laravel file manager tinymce
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('home', 'HomeController@index')->name('home');
Route::resource('profile', 'ProfileController');
Route::get('chapters', 'ChapterController@index')->name('chapter.index');
Route::resource('books/{book}/chapter', 'ChapterController')->except(['index']);
Route::resource('books', 'BookController');
Route::post('bookmark', 'BookController@saveBookmark');
Route::delete('bookmark', 'BookController@removeBookmark');


Route::get('/webcrawler', 'WebCrawlerController@index')->name('webcrawler.index');
Route::post('/webcrawler/book', 'WebCrawlerController@fetchBook')->name('webcrawler.book');
Route::post('/webcrawler/book/save', 'WebCrawlerController@saveBook');
Route::post('/webcrawler/chapter/save', 'WebCrawlerController@saveChapters');
Route::get('/webcrawler/chapter/{url}', 'WebCrawlerController@fetchChapter')->name('webcrawler.chapter')->where('url', '.*');