@extends('adminlte.master')

@section('title', 'Home')

@section('content')
@if(session('success'))     
<div class="alert alert-success">{{ session('success') }}  </div>
@endif                      
<div class="card">
      <div class="card-header">
        <h3 class="card-title">Title</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <p>Body</p>
      </div>
</div>
@endsection