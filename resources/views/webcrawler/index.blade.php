@extends('adminlte.master')

@section('title', 'Web Scrap' )

@section('content')
    <div class="mx-2 mt-2">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Web Scrap</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/webcrawler/book" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <p>ex: https://daonovel.com/novel/reincarnation-of-the-strongest-sword-god-webnovel/</p>
                        <input type="text" class="form-control" id="link" value="{{ old('link', '') }}"
                            name="link" placeholder="Insert link" required>
                            @error('link')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
