@extends('adminlte.master')

@section('title', 'Home')

@section('content')     
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{$chapter['title']}}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            {!! $chapter['content'] !!}
        </div>
    </div>
</div>
@endsection