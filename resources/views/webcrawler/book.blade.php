
@extends('adminlte.master')

@section('title', 'Web Scrap' )

@push('css')
    <link rel="stylesheet" href="{{ asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endpush

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">{{$book['title']}}</h3>
    </div>

    <div class="card-body">
        <div class="d-flex justify-content-between">
            <div class="p-2">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th style="width:120px;">Authors</th>
                            <td>{{ $book['authors'][0] }}</td>
                        </tr>
                        <tr>
                            <th style="width:120px;">Summary</th>
                            <td>{!! $book['summary'] !!}</td>
                        </tr>
                        <tr>
                            <th>Genres</th>
                            <td>
                                @forelse($book['genres'] as $tag)
                                    <button class="btn btn-primary">{{ $tag }}</button>
                                @empty
                                    No Tags
                                @endforelse
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="p-2">
                <form action="{{ action('WebCrawlerController@saveBook') }}" method="POST">
                    @csrf
                    <input id="book_data" type="hidden" value="{{ json_encode(['title'=>$book['title'], 'summary'=>$book['summary'], 'genres'=>$book['genres']]) }}" name="book">
                    <button id="save_book" type="submit" class="btn btn-primary">Pirate This Book <br>And All Selected Chapter</button>
                </form>
            </div>
        </div>
            <div class="p-2">
                <button id="select_all" type="button" class="btn btn-default">Select All</button>
                <button id="clear_all" type="button" class="btn btn-default">Clear</button>
            </div>
        </div>
        <div class="p-2">
            <table id="chapter-list" class="table table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th style="width: 10px">#</th>
                        <th>Chapter</th>
                        <th>Link</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse( array_reverse($book['chapters']) as $key => $chapter )
                        <tr style="transform:rotate(0);">
                            <td><input type="checkbox" name="chapter[]" value="{{ $chapter['url'] }}"></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $chapter['title'] }}</td>
                            <td><a href="/webcrawler/chapter/{{ rawurlencode($chapter['url']) }}">{{ $chapter['url'] }}</a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3"> No Chapter Data Found. </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script>
    var table = $("#chapter-list").DataTable();
    $('#select_all').click(function(){
        table.$("input[type='checkbox']").prop('checked', true);
    })
    $('#clear_all').click(function(){
        table.$("input[type='checkbox']").prop('checked', false);
    })
    
    $('#save_book').click(function (){
        var checked = [];
        table.$("input[type='checkbox']").each(function() {
            if (this.checked) {
                checked.push( $(this).parent().parent().find('td:last-child').children().text() );
            };
        }); 
        var book = JSON.parse($('#book_data').val());
        book['chapters'] = checked;
        $('#book_data').prop('value', JSON.stringify(book));
    });

    </script>
@endpush