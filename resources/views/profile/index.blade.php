@extends('adminlte.master')

@section('title', 'Profiles')

@section('content')
<div class="ml-3 mt-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Profiles</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th style="width: 20%">Username</th>
                        <th style="width: 40%">Foto</th>
                        <th style="width: 40%">Full Name</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse( $profiles as $key => $profile )
                        <tr style="transform:rotate(0);">
                            <td>{{ $profile->user->id }}</td>
                            <td><a class="stretched-link" href="{{ route('profile.show', ['profile'=>$profile->user->name]) }}">{{ $profile->user->name }}</a></td>
                            <td>{{ $profile->photo }}</td>
                            <td>{{ $profile->fullname }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4"> No Profile Data Found. </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
