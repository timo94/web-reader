@extends('adminlte.master')

@section('title', '@'. $user->name .' edit')

@push('css')
<style>
    .profile td,
    .profile th {
        padding: 0 .5rem;
    }

    .profile th {
        text-align: right;
    }

    .table td a {
        display: inline-block;
        height:100%;
        width:100%;
    }
</style>
@endpush

@section('content')
@if(session('success'))
<div class="alert alert-success">
    {{session('success')}}
</div>
@endif
<form role="form" action="{{ route('profile.update', ['profile'=>Auth::user()->name]) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="card">
        <div class="card-header d-flex align-items-center justify-content-between">
            <h3 class="card-title">{{ '@'. $user->name }}</h3>
            @if ( Auth::user() == $user )
            <input class="btn btn-primary m-2" style="position:absolute; right:0;" type="submit" value="Update">
            @endif
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="image mr-2">
                    @if ( $user->profile->photo )
                    <img src="{{ $user->profile->photo }}" style="height:200px; width:200px" alt="Profile Photo">
                    @else
                    <h6 class="text-center align-middle" style="height:200px; width:200px; background-color:rgb({{rand(128,240)}},{{rand(128,240)}},{{rand(128,240)}}); font-size:150px;">
                        {{ strtoupper($user->name)[0] }}
                    </h6>
                    @endif
                </div>
                <div class="col">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th style="width:120px;">#</th>
                                <td>{{ $user->id }}</td>
                            </tr>
                            <tr>
                                <th>Created at:</th>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                            <tr>
                                <th>Username:</th>
                                <td>
                                    <input type="text" id="name" name="name" value='{{ $user->name }}'>
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </td>
                            </tr>
                            <tr>
                                <th>Fullname:</th>
                                <td><input type="text" id="fullname" name="fullname" value='{{ $user->profile->fullname ?? "" }}'></td>
                            </tr>
                            <tr>
                                <th>About:</th>
                                <td><input type="text" id="about" name="about" value='{!! $user->profile->about ?? "" !!}'></td>
                            </tr>
                            <tr>
                                <th>Photo:</th>
                                <td><input type="file" id="photo" name="photo" value='{{ $user->profile->photo ?? "" }}'></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</form>
<!-- tabs -->
<div class="card">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="bookmarks-tab" data-toggle="tab" href="#bookmarks" role="tab" aria-controls="bookmarks" aria-selected="true">Bookmarks</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="book-tab" data-toggle="tab" href="#book" role="tab" aria-controls="book" aria-selected="false">Written Books</a>
        </li>
    </ul>

    <div class="card-body">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="bookmarks" role="tabpanel" aria-labelledby="bookmarks-tab">
                <table id="chapter-list" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                            <th>Last Read</th>
                            <th>Latest Chapter</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $user->bookmarks as $key => $book )
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td><a href="{{ route('books.show', ['book'=>$book['id']])}}">{{ $book['title'] }}</a></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4"> This User haven't bookmarked any book. </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="book" role="tabpanel" aria-labelledby="book-tab">
                <table id="chapter-list" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse( $user->writtenBooks as $key => $book )
                            <tr style="transform:rotate(0);">
                                <td>{{ $key+1 }}</td>
                                <td><a href="{{ route('books.show', ['book'=>$book['id']])}}">{{ $book['title'] }}</a></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2"> This User haven't written any book. </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection