@extends('adminlte.master')

@section('title', 'Chapters')

@section('content')
<div class="mt-3 mx-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Chapter List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Published Date</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($chapters as $key => $item)
                    <tr style="transform:rotate(0);">
                        <td>{{ $key + 1 }}</td>
                        <td><a class="stretched-link" href="{{ route('chapter.show', ['book'=>$item->book_id, 'chapter'=>$item->id ])}}">{{ $item->title }}</a></td>
                        <td><div style="max-height: 5rem;overflow: hidden;text-overflow: ellipsis;">{!! $item->content !!}
                        </div></td>
                        <td>{{ $item->published_date }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6" align="center">No Chapter</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

</div>
@endsection