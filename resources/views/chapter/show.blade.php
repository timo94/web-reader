@extends('adminlte.master')

@push('css')
<style>
    .navigation {
        position: fixed;
        top: 50px;
        bottom: 0;
        margin: 0;
        max-width: 100px;
        min-width: 50px;
        display: flex;
        justify-content: center;
        align-content: center;
        flex-direction: column;
        font-size: 40px;
        color: #ccc;
        text-align: center;
    }

    .navigation.next {
        right: 0;
    }
</style>
@endpush

@section('content')
@if ($chapter->previous())
    <div class="navigation prev">
        <a href="{{ route('chapter.show', ['book'=>$chapter->book->id, 'chapter'=>$chapter->previous()->id ])}}"><i class="fas fa-arrow-left"></i></a>
    </div>
@endif
@if ($chapter->next())
    <div class="navigation next">
        <a href="{{ route('chapter.show', ['book'=>$chapter->book->id, 'chapter'=>$chapter->next()->id ])}}"><i class="fas fa-arrow-right"></i></a>
    </div>
@endif

<div class="container px-5">
    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <div class="p-0">
                    <h3>{{ $chapter->title }}</h3>
                </div>
                <div class="p-0 d-flex">
                @guest
                @else
                    @if( $chapter->book->bookmarked(Auth::user()->id) )
                        <form action="{{ action('BookController@removeBookmark') }}" method="POST">
                        @csrf
                        @method("DELETE")
                            <input id="checked_chapters" type="hidden" value="{{$chapter->book->id}}" name="book_id">
                            <input id="checked_chapters" type="hidden" value="{{$chapter->id}}" name="chapter_id">
                            <button id="fetch_chapters" type="submit" class="btn btn-primary">
                                <i class="fas fa-bookmark fa-sm"></i>
                            </button>
                        </form>
                    @else
                        <form action="{{ action('BookController@saveBookmark') }}" method="POST">
                        @csrf
                            <input id="checked_chapters" type="hidden" value="{{$chapter->book->id}}" name="book_id">
                            <input id="checked_chapters" type="hidden" value="{{$chapter->id}}" name="chapter_id">
                            <button id="fetch_chapters" type="submit" class="btn btn-primary">
                                <i class="far fa-bookmark fa-sm"></i>
                            </button>
                        </form>
                    @endif
                    @if(Auth::user()->id == $chapter->book->author->id)
                    <form action="{{route('chapter.edit', ['book'=>$chapter->book->id, 'chapter'=>$chapter->id])}}">
                        <button id="edit_book" type="submit" class="btn btn-primary ml-2">
                            <i class="fas fa-pencil-alt fa-sm"></i>
                            Edit
                        </button>
                    </form>
                    @endif
                @endguest
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-text">
            {!! $chapter->content !!}
            </div>
        </div>
        <div class="card-footer">
            Published on : {{ $chapter->published_date }}
        </div>
    </div>
</div>
@endsection