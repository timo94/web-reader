@extends('adminlte.master')
@push('script-tiny')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush
@section('title', $chapter->title )

@section('content')
<div class="mx-2 mt-2">
    <div class="card card-primary">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <div class="p-0">
                        <h4>Edit Chapter</h4>
                    </div>
                    <div class="p-0">
                        <form action="{{route('chapter.destroy', ['book'=>$chapter->book->id, 'chapter'=>$chapter->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" value="delete" class="btn btn-danger btn-sm">Delete this Chapter</button>
                        </form>
                    </div>
                </div>
            </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="{{route('chapter.update', ['book'=>$chapter->book->id, 'chapter'=>$chapter->id])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" value="{{ old('title', $chapter->title) }}" name="title" placeholder="Insert title" required>
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    {{-- <input type="text" class="form-control" id="content" value="{{ old('content', $chapter->content) }}" name="content" placeholder="Insert content" required> --}}
                    <textarea name="content" class="form-control my-editor">{!! old('content', $chapter->content ?? '') !!}</textarea>
                    @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="published_date">Published Date</label>
                    <input type="date" class="form-control" id="published_date" value="{{ old('published_date', $chapter->published_date) }}" name="published_date" placeholder="Insert Published Date " required>
                    @error('published_date')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>

</div>
@endsection

@push('scripts')
    <script>
        var editor_config = {
            path_absolute: "/",
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback: function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(
                    'body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document
                    .getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        };

        tinymce.init(editor_config);

    </script>
@endpush