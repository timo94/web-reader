@extends('adminlte.master')

@section('title', $book->title)

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <div class="p-0">
                    <h3>{{ $book->title }}</h3>
                </div>
                <div class="p-0 d-flex">
                @guest
                @else
                    @if( $book->bookmarked(Auth::user()->id) )
                        <form action="{{ action('BookController@removeBookmark') }}" method="POST">
                        @csrf
                        @method("DELETE")
                            <input id="checked_chapters" type="hidden" value="{{$book->id}}" name="book_id">
                            <input id="checked_chapters" type="hidden" value="" name="chapter_id">
                            <button id="fetch_chapters" type="submit" class="btn btn-primary">
                                <i class="fas fa-bookmark fa-sm"></i>
                            </button>
                        </form>
                    @else
                        <form action="{{ action('BookController@saveBookmark') }}" method="POST">
                        @csrf
                            <input id="checked_chapters" type="hidden" value="{{$book->id}}" name="book_id">
                            <input id="checked_chapters" type="hidden" value="" name="chapter_id">
                            <button id="fetch_chapters" type="submit" class="btn btn-primary">
                                <i class="far fa-bookmark fa-sm"></i>
                            </button>
                        </form>
                    @endif
                    @if(Auth::user()->id == $book->author->id)
                    <form action="{{route('books.edit', ['book'=>$book->id])}}">
                        <button id="edit_book" type="submit" class="btn btn-primary ml-2">
                            <i class="fas fa-pencil-alt fa-sm"></i>
                            Edit
                        </button>
                    </form>
                    @endif
                @endguest
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-text">
            {!! $book->summary !!}
            </div>
        </div>
        <div class="card-footer">
            Tags :
            @forelse($book->tags as $tag)
                <button class="btn btn-primary">{{ $tag->name }}</button>
            @empty
                No Tags
            @endforelse
        </div>
    </div>

    
    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <div class="p-0">
                    <h3>Chapters</h3>
                </div>
                @guest
                @else
                    @if(Auth::user()->id == $book->author->id)
                    <div class="p-0">
                        <a class="btn btn-primary" href="{{route('chapter.create', ['book' => $book->id])}}"> Write New Chapter</a>
                    </div>
                    @endif
                @endguest
            </div>
        </div>
        <div class="card-body">
            <table id="chapter-list" class="table table-borderless">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Chapter</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse( $book->chapters as $key => $chapter )
                        <tr style="transform:rotate(0);">
                            <td>{{ $key+1 }}</td>
                            <td><a class="stretched-link" href="{{route('chapter.show', ['book' => $book->id, 'chapter' => $chapter->id])}}">{{ $chapter->title }}</a></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3"> No Chapter Data Found. </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection
