@extends('adminlte.master')

@section('title', 'Books')

@section('content')
    <div class="row">
        <div class="col-md-6 col-lg-4 col-sm-12 p-2">
            <div class="card h-100" style="transform:rotate(0);">
                <div class="align-middle d-flex justify-content-center align-items-center" style="height:100%;">
                    <div>
                        <i class="fas fa-pencil-alt fa-5x" style="width:100%; text-align:center;"></i>
                        <a class="stretched-link" style="display:block;" href="{{ route('books.create') }}">Write Book</a>
                    </div>
                </div>
            </div>
        </div>
    @forelse ($books as $key => $item)
        <div class="col-md-6 col-lg-4 col-sm-12 p-2">
            <div class="card h-100" style="transform:rotate(0);">
                <div class="card-header">
                        <h3 class="card-title">{{ $item->title }}</h3>
                </div>
                <div class="card-body" style="height: 18rem; overflow: hidden; text-overflow: ellipsis;">
                    <div class="card-text">
                        {!! $item->summary !!}
                    </div>
                </div>
                <div class="card-footer">
                    <a class="stretched-link" href="{{ route('books.show', ['book'=>$item->id]) }}">
                        Read
                    </a>
                </div>
            </div>
        </div>
    @empty
    @endforelse
    </div>
@endsection
