<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="{{ asset('/adminlte/dist/img/AdminLTELogo.png')}} " alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Web Reader</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    @guest
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-around">
      <div class="nav">
        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
      </div>
      @if (Route::has('register'))
      <div class="nav">
        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
      </div>
      @endif
    </div>
    @else
    <div class="user-panel my-3 pb-3">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link py-2 px-0">
            <div class="image mr-2">
              @if ( Auth::user()->profile->photo )
              <img src="{{ Auth::user()->profile->photo }}" style="height:33.6px; width:33.6px" class="img-circle" alt="Profile Photo">
              @else
              <h3 class="img-circle text-center align-middle" style="height:33.6px; width:33.6px; background-color:rgb({{rand(10,180)}},{{rand(10,180)}},{{rand(10,180)}})">
                {{ strtoupper(Auth::user()->name)[0] }}
              </h3>
              @endif
            </div>
            <p class="align-middle">
              <p style="display:inline; font-size:1.3rem;">
                {{ '@'. Auth::user()->name }}
              </p>
              <i class="fas fa-angle-left right" style="top:1.2rem;"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('profile.show', ['profile'=>Auth::user()->name]) }}">
                <i class="far fa-circle nav-icon"></i>
                <p>Profile</p>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                <i class="far fa-circle nav-icon"></i>
                <p>{{ __('Logout') }}</p>
              </a>

              <form id="logout-form" style="z-index:5;" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    @endguest
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview menu-open">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Pages
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('profile.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Profiles</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('books.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Books</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('chapter.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Chapters</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('webcrawler.index') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>WebScrap</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>