# Web Reader

Sanber Laravel Web Development batch 18 Final Project

![Alt text](public/diagram/erd.png?raw=true "ERD")

Panduan:
1. Setiap User memiliki Profile
2. User dapat membaca atau menulis buku
3. Buku dapat memiliki banyak chapter
4. User dapat menyimpan daftar buku bacaan 
   dan chapter terakhir yang dibaca
5. Buku dapat memiliki banyak tag
6. 1 jenis Tag dapat di sisipkan ke banyak buku
7. Chapter memiliki tanggal publish, 
   jika tanggal publish lebih lama dari tanggal hari ini, 
   maka dapat dilihat semua user,
   kalau belum dipublish hanya dapat dilihat oleh penulis


Run Composer Update to Install All Required Package
List of package used
- Auth
- maatwebsite/excel
- barryvdh/laravel-dompdf
- realrashid/sweet-alert


